# Version: 0.0.1
FROM ubuntu:18.04
LABEL maintainer="bpetkov13@gmail.com"
RUN curl -sL https://repos.influxdata.com/influxdb.key | apt-key add -
RUN source /etc/lsb-release
RUN echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | tee /etc/apt/sources.list/influxdb.list
RUN apt-get update; apt-get install influxdb